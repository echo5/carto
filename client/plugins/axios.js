import axios from 'axios'

let options = {}

if (process.env.apiUrl) {
	options.baseURL = `${process.env.apiUrl}`
} else {
	options.baseURL = 'https://carto.echo5digital.com/wp-json/wp/v2'
}

export default axios.create(options)
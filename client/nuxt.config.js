require('dotenv').config()
const axios = require('axios')
const apiUrl = process.env.apiUrl || 'https://carto.echo5digital.com/wp-json/wp/v2'

module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'carto',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Carto imagine that...' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Montserrat|Open+Sans'}
    ]
  },
  generate: {
    routes: function () {
      return axios.get(`${apiUrl}/kb-articles?per_page=100`)
      .then((res) => {
        return res.data.map((article) => {
          return '/knowledge-base/' + article.slug
        })
      })
    }
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#72c96a' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  modules: [
     '@nuxtjs/dotenv',
  ]
}

# carto

Knowledgebase for Carto built with Vue.js on the frontend and the WP API for backend.

### Why WordPress

Intuitive interface for non-technical users.  Large ecosystem of plugins available.  The WordPress API has come a long way and lets us take advantage of all the great features in WordPress without the ugly.

### Why Nuxt (Vue.js)

Decoupling our CMS from frontend reduces lock-in.  We also get a better development experience with webpack features, like hot-reloading and can consume content from any API.

Nuxt provides SSR out of the box so documentation remains search engine friendly.  Vue single-file components make our application extremely modular.

## Setup

### Server Setup

Visit the URL that leads to your server path and follow the WP installation instructions to create a database and config.

Make sure to replace your site URL in the wp-config.php file.

``` php
// wp-config.php
define('WP_HOME','http://yourdomain.com/path/to/server');
define('WP_SITEURL','http://yourdomain.com/path/to/server');
```

Replace your newly created database with the sql dump file (carto.sql) found under the server directory.  This contains the dummy content, custom post types, and activated plugins.

### Client Setup
Create a dotenv file (.env) under the clients folder with the apiUrl

```bash
# .env
apiUrl=http://yourdomain.com/path/to/server/wp-json/wp/v2
```
Developing and buliding on the frontend is done under the client directory.

``` bash
# cd to client directory
cd client

# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

## Working with content
Content is pulled from the** KB Articles** post type.  Currently the frontend makes use of the following items:
- Title
- Content
- Slug
- Featured image (svg) for top-level articles
- Categories
- Tags


## Making this production ready
- Add tests
- Add auto-generated sitemap
- Cache API requests
- Remove WP core and uploads from version control
- Move custom post types to theme